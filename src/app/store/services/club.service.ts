import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { Club } from '../../types/club';

@Injectable({ providedIn: 'root' })
export class ClubService extends EntityCollectionServiceBase<Club> {
  public static storeName = 'Club';
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(ClubService.storeName, serviceElementsFactory);
  }
}
