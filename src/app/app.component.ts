import { Component, OnInit } from '@angular/core';
import { ClubService } from './store/services/club.service';
import { ClubDataService } from './services/club-data.service';

@Component({
  selector: 'shf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private clubService: ClubService,
  ) { }

  ngOnInit() {
    ClubDataService.handleRequest(this.clubService.getAll());
  }

}
