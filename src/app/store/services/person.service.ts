import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { Person } from '../../types/person';

@Injectable({ providedIn: 'root' })
export class PersonService extends EntityCollectionServiceBase<Person> {
  public static storeName = 'Person';
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(PersonService.storeName, serviceElementsFactory);
  }
}
