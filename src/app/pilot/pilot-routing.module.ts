import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LicenseComponent } from './license/license.component';
import { RenewLicenseComponent } from './renew-license/renew-license.component';

const routes: Routes = [
  { path: 'license', component: LicenseComponent },
  { path: 'license-application', component: RenewLicenseComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PilotRoutingModule { }
