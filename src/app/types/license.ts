import { NumberValueAccessor } from '@angular/forms';

export interface License {
  personId: number;
  licenseNbr: number;
}

export interface Skill {
  id: number;
  swedish: string;
  english: string;
}

export interface PilotSkill {
  id: number;
  licenseNbr: number;
  skillId: number;
  issueDate: string; // Date;
  issuingInstructorLicNbr: number;
  revokeDate: string; // Date;
}

export interface LicenseApplication {
  id: number;
  licenseNbr: number;
  year: number;
  level: string;
  clubId: number;
  downgradeToElev: number; // boolean;
  needFlightTest: number; // boolean;
  timestampPilot: string;
  timestampClub: string;
  approvedClub: number; // boolean;
  timestampOffice: string;
  approvedOffice: number; // boolean;
  timestampInstructor: string;
  comment: string;
  commentOffice: string;
}
