import { AfterContentInit, Directive, ElementRef, Input } from '@angular/core';

@Directive({
     selector: '[shfAutoFocus]'
})
export class AutoFocusDirective implements AfterContentInit {

     public constructor(private el: ElementRef) {     
     }

     public ngAfterContentInit() {
         setTimeout(() => {
             this.el.nativeElement.focus();
         });
     }
}