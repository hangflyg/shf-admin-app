import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { combineLatest, Observable, Subscription } from 'rxjs';

import { MatStepper } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';

import { select, Store } from '@ngrx/store';
import { AuthUser, userSelector } from '@holmby/auth';
import { map, tap } from 'rxjs/operators';
import { Person } from 'src/app/types/person';
import { PersonService } from 'src/app/store/services/person.service';
import { formatDate } from '@angular/common';
import { IncidentReportService } from 'src/app/store/services/incident.service';
import { ActivatedRoute } from '@angular/router';

export function creatPilotFormValidator(): ValidatorFn {
  return (form: FormGroup): ValidationErrors | null => {
    const anonymous = form.get("anonymousPilot").value;
    const licenseNbr = form.get("licenseNbr").value;
    if ((!anonymous) && (!licenseNbr)) {
      return { missingLicenseNbr: true };
    }
    return null;
  }
}

@Component({
  selector: 'shf-incident',
  templateUrl: './incident.component.html',
  styleUrls: ['./incident.component.scss']
})
export class IncidentComponent implements OnInit, OnDestroy {
  Mode = Object.freeze({
    edit: Symbol('edit'),
    create: Symbol('create')
  })
  States = Object.freeze({
    edit: Symbol('edit'),
    sending: Symbol('sending'),
    saved: Symbol('saved'),
  })
  @ViewChild('stepper', { static: false }) stepper: MatStepper;
  reporterForm: FormGroup = this.fb.group({
    anonymousReporter: [''],
    name: [''],
    email: [''],
    phone: [''],
  });
  incidentForm: FormGroup = this.fb.group({
    date: [new Date(), Validators.required],
    time: ['00:00', Validators.required],
    reasonForFlight: [''],
    description: ['', Validators.required]
  });
  pilotForm: FormGroup = this.fb.group({
    anonymousPilot: ['anonymous'],
    licenseNbr: [''],
    club: [''],
    age: [''],
    licenseLevel: [''],
    totalFlightTime: [''],
    recentFlightTime: [''],
    gliderFlightTime: ['']
  }, {
    validators: [creatPilotFormValidator()]
  });
  injuriesForm: FormGroup = this.fb.group({
    injuriesPilot: [''],
    damageEquipment: ['']
  });
  equipmentForm: FormGroup = this.fb.group({
    harness: [''],
    glider: [''],
    tandem: [''],
    poweredHG: ['']
  });
  locationForm: FormGroup = this.fb.group({
    location: ['']
  });
  commentForm: FormGroup = this.fb.group({
    commentReporter: ['']
  });

  authUser$: Observable<AuthUser>;
  loggedinPerson$: Observable<Person>;
  subscriptions: Subscription[] = [];
  state = this.States.edit;

  sub; id;
  mode = this.Mode.edit;

  constructor(
    private fb: FormBuilder,
    private personService: PersonService,
    private incidentReportService: IncidentReportService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private store: Store<any>
  ) {
    // authUser
    let lastUser;
    this.authUser$ = this.store.pipe(select(userSelector)).pipe(
      tap(authUser => {
        if (authUser) {
          this.pilotForm.patchValue({ licenseNbr: authUser.payload.lic });
          this.reporterForm.patchValue({ name: authUser.name });
        }
        // will be called once for each subscription
        // only fetch server data once (distinct will not work)
        if (authUser && lastUser !== authUser.payload.sub) {
          lastUser = authUser.payload.sub;
        }
      })
    );

    // loggedInPerson
    this.loggedinPerson$ = combineLatest(this.authUser$, this.personService.entityMap$).pipe(
      map(([authUser, personMap]) => (!authUser || !personMap) ? null : personMap[authUser.payload.sub]),
      tap(person => {
        if (person) {
          this.reporterForm.patchValue({ email: person.email, phone: person.mobile })
        }
      })
    );
    this.subscriptions.push(this.loggedinPerson$.subscribe());
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if(params.id) {
        this.mode = this.Mode.edit;
        this.incidentReportService.getByKey(params.id)
      } else {
        this.mode = this.Mode.create;
      }
    });
  }

  onAnonymousReporterChanged(event) {
    if (event.checked) {
      this.stepper.next();
    }
  }

  onPreview() {
    if (!this.incidentForm.valid) {
      this.incidentForm.markAllAsTouched();
      this.stepper.selectedIndex = 1;
      this.snackBar.open('Fyll i beskrivningen av händelsen', 'ok', {
        duration: 3000
      });
    } else if (!this.pilotForm.valid) {
      this.snackBar.open('Ange licensnummer eller välj annonym', 'ok', {
        duration: 3000
      });
      this.pilotForm.markAllAsTouched();
      this.stepper.selectedIndex = 2;
    } else {
      this.stepper.selectedIndex = 7;
    }
  }

  onSubmit() {
    if (!this.incidentForm.valid) {
      this.incidentForm.markAllAsTouched();
      this.stepper.selectedIndex = 1;
      this.snackBar.open('Fyll i beskrivningen av händelsen', 'ok', {
        duration: 3000
      });
    } else if (!this.pilotForm.valid) {
      this.pilotForm.markAllAsTouched();
      this.stepper.selectedIndex = 2;
      this.snackBar.open('Ange licensnummer eller välj annonym', 'ok', {
        duration: 3000
      });
    } else {
      this.state = this.States.sending;
      let value: any = {
        id: null,
        reporterName: null,
        reporterEmail: null,
        reporterPhone: null,
        timestampOfEvent: this.incidentForm.value.date,
        typeOfFlight: this.incidentForm.value.reasonForFlight,
        description: this.incidentForm.value.description,
        licenseNbr: null,
        club: null,
        age: null,
        licenseLevel: null,
        totalFlightTime: null,
        resentFlightTime: this.pilotForm.value.recentFlightTime,
        gliderFlightTime: this.pilotForm.value.gliderFlightTime,
        damagePilot: this.injuriesForm.value.injuriesPilot,
        damageGlider: this.injuriesForm.value.damageEquipment,
        glider: this.equipmentForm.value.glider,
        harness: this.equipmentForm.value.harness,
        location: this.locationForm.value.location,
        commentReporter: this.commentForm.value.commentReporter,
        createTimestamp: new Date(),
        lastUpdate: new Date()
      }
      value.timestampOfEvent = formatDate(this.incidentForm.value.date, 'yyyy-MM-dd', 'en-US') +
        ' ' + this.incidentForm.value.time;
      if (!this.reporterForm.value.anonymousReporter) {
        value.reporterName = this.reporterForm.value.name;
        value.reporterEmail = this.reporterForm.value.email;
        value.reporterPhone = this.reporterForm.value.phone;
      }
      if (this.pilotForm.value.anonymousPilot) {
        value.club = this.pilotForm.value.club;
        value.age = this.pilotForm.value.age;
        value.licenseLevel = this.pilotForm.value.licenseLevel;
        value.totalFlightTime = this.pilotForm.value.totalFlightTime;
      } else {
        value.licenseNbr = this.pilotForm.value.licenseNbr;
      };
      // replace empty strings with null
      Object.keys(value).forEach(key => {
        if (!value[key]) {
          value[key] = null;
        }
      })

      const tmp = this.incidentReportService.add(value).subscribe(_ => {
        this.state = this.States.saved;
        this.snackBar.open('Rapporten har skickats in', 'ok', {
          duration: 3000
        });
        tmp.unsubscribe();
        //      console.log('data: ' + JSON.stringify(value));
      });
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
