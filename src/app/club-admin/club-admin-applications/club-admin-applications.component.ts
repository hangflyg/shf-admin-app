import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { LicenseApplication } from '../../types/license';
import { LicenseApplicationService } from '../../store/services/license-application.service';
import { AuthUser } from '@holmby/auth';
import { Person } from '../../types/person';
import { ClubDataService } from 'src/app/services/club-data.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'shf-club-admin-applications',
  templateUrl: './club-admin-applications.component.html',
  styleUrls: ['./club-admin-applications.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ClubAdminApplicationComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  columnsToDisplay = ['approve', 'reject', 'year', 'level', 'name', 'comment'];
  expandedElement: any | null;
  dataSource = new MatTableDataSource<any>([]);

  authUser$: Observable<AuthUser> = this.prefetchService.authUser$;
  applications$: Observable<{application: LicenseApplication, person: Person}[]> = this.prefetchService.clubApplications$;
  applicationCount$ = this.applications$.pipe(
    map(list => list.length)
  );

  private subsriptions: Subscription[] = [];
  constructor(
    private prefetchService: ClubDataService,
    private licenseApplicationService: LicenseApplicationService,
  ) { }

  ngOnInit() {
    this.subsriptions.push(this.prefetchService.clubApplications$.subscribe(list => {
      this.dataSource.data = list;
      if (this.paginator) {
        this.dataSource.paginator = this.paginator;
      }
    }));
  }

  ngOnDestroy() {
    this.subsriptions.forEach(s => s.unsubscribe());
    this.subsriptions = [];
  }

  handleApplication(data, approve) {
    const application = {
      id: data.application.id,
      timestampClub: new Date().toISOString().slice(0, 19).replace('T', ' '),
      approvedClub: approve
    };
    ClubDataService.handleRequest(this.licenseApplicationService.update(application));
  }
}
