import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { AuthUser, userSelector } from '@holmby/auth';
import { ClubDataService } from '../services/club-data.service';

@Component({
  selector: 'shf-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  clubAdminName = 'clubadmin';
  officeAdminName = 'office';
  saftyAdminName = 'safety';
  authUser$: Observable<AuthUser> = this.store.pipe(select(userSelector));

  isPilot$: Observable<boolean> = this.authUser$.pipe(
    map((user: AuthUser) => user ? !!user.payload.lic : false),
  );

  clubAdmin$: Observable<boolean> = this.authUser$.pipe(
    map((user: AuthUser) => user ? !!user.privileges[this.clubAdminName] : false),
  );
  clubNbrApplications$ = this.prefetchService.clubApplications$.pipe(
    map(list => list.length)
  );

  isOfficeAdmin$: Observable<boolean> = this.authUser$.pipe(
    map((user: AuthUser) => user ? !!user.privileges[this.officeAdminName] : false),
  );
  officeNbrApplications$ = this.prefetchService.officeApplications$.pipe(
    map(list => list.length)
  );

  isSaftyAdmin$: Observable<boolean> = this.authUser$.pipe(
    map((user: AuthUser) => user ? !!user.privileges[this.saftyAdminName] : false),
  );

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    constructor(
      private store: Store<any>,
      private breakpointObserver: BreakpointObserver,
      private prefetchService: ClubDataService
  ) { }
}
