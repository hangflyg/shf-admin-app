import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { LicenseApplication } from '../../types/license';

@Injectable({ providedIn: 'root' })
export class LicenseApplicationService extends EntityCollectionServiceBase<LicenseApplication> {
  public static storeName = 'LicenseApplication';
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(LicenseApplicationService.storeName, serviceElementsFactory);
  }
}
