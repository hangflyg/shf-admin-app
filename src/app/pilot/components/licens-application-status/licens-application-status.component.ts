import { Component, OnInit, Input } from '@angular/core';
import { Observable, combineLatest, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { LicenseApplication } from 'src/app/types/license';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'shf-licens-application-status',
  templateUrl: './licens-application-status.component.html',
  styleUrls: ['./licens-application-status.component.scss']
})
export class LicensApplicationStatusComponent implements OnInit {
  @Input() application: LicenseApplication;
  @Input() insurances;

  formGroup: FormGroup = this.fb.group({
    insurance: null
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
  }

  formatDate(date: string) {
    if (!date) {
      return '';
    } else {
      return date.substring(0, 16);
    }
  }

  formatDecision(timestamp: string, decistion: boolean) {
    return 'hepp';
  }

}
