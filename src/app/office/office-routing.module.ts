import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfficeApplicationComponent } from './office-application/office-application.component';
import { PilotSkillComponent } from './pilot-skill/pilot-skill.component';

const routes: Routes = [
  { path: 'applications', component: OfficeApplicationComponent },
  { path: 'pilot-skill', component: PilotSkillComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfficeRoutingModule { }
