import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClubAdminApplicationComponent } from './club-admin-applications/club-admin-applications.component';

const routes: Routes = [
  { path: 'applications', component: ClubAdminApplicationComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClubAdminRoutingModule { }
