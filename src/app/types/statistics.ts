export interface Question {
  id: number;
  text: string;
  answer_type: string;
}

export interface Answer {
  qId: number;
  year: number;
  personId: number;
  value: number;
  text: string;
}

export function answerKey(obj: Answer) {
    return 'q' + obj.qId + 'y' + obj.year + 'p' + obj.personId;
}

