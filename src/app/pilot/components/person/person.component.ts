import { Component, EventEmitter, Input, OnInit, OnChanges, Output, SimpleChange } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Person } from 'src/app/types/person';

@Component({
  selector: 'shf-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnChanges {
  @Input() person: Person;

  formGroup: FormGroup = this.fb.group({
    id: ['', Validators.required],
    personNumber: [''],
    givenName: ['', Validators.required],
    familyName: ['', Validators.required],
    street: ['', Validators.required],
    zip: ['', Validators.required],
    city: ['', Validators.required],
    country: ['', Validators.required],
    mobile: ['', Validators.required],
    email: ['', Validators.required],
    protectedIdentity: [''],
    iol: [''],
//    password: [''],
    contactPersonName: [''],
    contactPersonPhone: [''],
  });

  constructor(private fb: FormBuilder) {
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    const personProp = changes.person;
    if (personProp) {
      if (personProp.currentValue) {
        this.formGroup.setValue(personProp.currentValue);
      } else {
        // person is null or undefined
        this.formGroup.reset();
      }
    }
  }
}
