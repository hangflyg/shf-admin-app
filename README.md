# SHF admin app

Web application for managing andministration in the swedish hang gliding association.


## Installation



## Angular
The web app is based on angular, angular material and ngrx store.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
The app must be located at:

```/shf/app/index.html```

The REST backend must be located at:

```/shf/api/index.php```


Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
