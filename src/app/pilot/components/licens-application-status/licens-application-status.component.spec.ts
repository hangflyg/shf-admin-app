import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicensApplicationStatusComponent } from './licens-application-status.component';

describe('LicensApplicationStatusComponent', () => {
  let component: LicensApplicationStatusComponent;
  let fixture: ComponentFixture<LicensApplicationStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicensApplicationStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicensApplicationStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
