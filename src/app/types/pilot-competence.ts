export class PilotCompetence {
  id: number;
  licensNumber: number;
  competence: string;
  issueDate: string; // Date
  issuingInstructorNumber: number;
  revokeDate: string; // Date
}

export class CompetenceState {
  [licensNumber: number]: PilotCompetence[]

  constructor(list: PilotCompetence[]) {
    list.forEach(comp => {
      if (!this[comp.licensNumber]) {
        this[comp.licensNumber] = [];
      }
      this[comp.licensNumber].push(comp);
    });
  }
}
