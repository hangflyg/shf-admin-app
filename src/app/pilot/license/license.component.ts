import { Component, OnInit } from '@angular/core';
import { LicenseApplicationService } from 'src/app/store/services/license-application.service';
import { map } from 'rxjs/operators';
import { ClubDataService } from 'src/app/services/club-data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'shf-license',
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.scss']
})
export class LicenseComponent implements OnInit {

  authUser$ = this.dataService.authUser$;
  applicationMap$ = this.licenseApplicationService.entities$.pipe(
    map(list => {
      const dict = {};
      list.forEach(appl => dict[appl.year]  = appl);
      return dict;
    })
  );

  constructor(
    private licenseApplicationService: LicenseApplicationService,
    private dataService: ClubDataService
  ) {
  }

  ngOnInit() {
  }

}
