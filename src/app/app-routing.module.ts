import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { IncidentComponent } from './incident/incident/incident.component';
import { ListIncidentsComponent } from './incident/list-incidents/list-incidents.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/pilot/license-application',
    pathMatch: 'full'
  },
  { path: 'incident', component: IncidentComponent },
  { path: 'incident/:id', component: IncidentComponent },
  { path: 'list-incidents', component: ListIncidentsComponent },

  { path: 'pilot', loadChildren: () => import('./pilot/pilot.module').then(m => m.PilotModule) },
  { path: 'club-admin', loadChildren: () => import('./club-admin/club-admin.module').then(m => m.ClubAdminModule) },
  { path: 'office', loadChildren: () => import('./office/office.module').then(m => m.OfficeModule) },

  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
