import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { License } from '../../types/license';
import { map, shareReplay } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class LicenseService extends EntityCollectionServiceBase<License> {
  public static storeName = 'License';
  // map: personId => lisenceNbr
  public readonly personMap$ = this.entities$.pipe(
    map((list) => {
      const map = {};
      list.forEach(license => map[license.personId] = license.licenseNbr);
      return map;
    }),
    shareReplay()
  );
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(LicenseService.storeName, serviceElementsFactory);
  }
}
