import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Observable, of, combineLatest } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { MatStepper } from '@angular/material';

import { userSelector, AuthUser } from '@holmby/auth';
import { Person } from '../../types/person';
import { PersonService } from '../../store/services/person.service';
import { SkillService } from '../../store/services/skill.service';
import { map, tap } from 'rxjs/operators';

import { PilotSkillService } from '../../store/services/pilot-skill.service';
import { QuestionService } from '../../store/services/question.service';
import { AnswerService } from '../../store/services/answer.service';

import { Answer } from '../../types/statistics';
import { Skill, PilotSkill, LicenseApplication } from '../../types/license';

import { PersonComponent } from '../components/person/person.component';
import { StatisticsComponent } from '../components/statistics/statistics.component';
import { LicenseOptionsComponent } from '../components/license-options/license-options.component';
import { LicenseApplicationService } from '../../store/services/license-application.service';
import { ClubDataService } from 'src/app/services/club-data.service';

@Component({
  selector: 'shf-pilot-license-application',
  templateUrl: './renew-license.component.html',
  styleUrls: ['./renew-license.component.scss']
})
export class RenewLicenseComponent implements OnInit {
  @ViewChild('stepper', {static: false}) stepper: MatStepper;
  @ViewChild(PersonComponent, {static: false})
  set setPersonComponent(c: PersonComponent) {
    setTimeout(() => { this.personForm = c ? c.formGroup : null; }, 0);
  }
  @ViewChild(StatisticsComponent, {static: false})
  set setStatisticsComponent(c: StatisticsComponent) {
    setTimeout(() => {
      this.statisticComponent = c;
      this.statisticsForm = c ? c.formGroup : null; }, 0);
  }
  @ViewChild(LicenseOptionsComponent, {static: false})
  set setApplicationComponent(c: LicenseOptionsComponent) {
    setTimeout(() => {
      this.licenseOptionsComponent = c;
    }, 0);
  }

  personForm: FormGroup;
  statisticsForm: FormGroup;
  statisticComponent: StatisticsComponent;
  licenseOptionsComponent: LicenseOptionsComponent;

  // TODO make year and insruance dynamic
  year = 2023;
  readonly memberFee = 350;
  readonly licenseFee = 220;
  readonly fees = this.memberFee + this.licenseFee;
  readonly insurances = [{id: 1, name: 'BAS', price: 200 + this.fees},
                {id: 2, name: 'STANDARD', price: 950 + this.fees} ,
                {id: 3, name: 'PLUS', price: 2035 + this.fees} ,
                {id: 4, name: 'försäkring via SSFF', price: this.fees} ];

  authUser$: Observable<AuthUser>;
  loggedinPerson$: Observable<Person>;
  licNbr$: Observable<number>;
  skills$: Observable<Skill[]>;
  pilotSkills$: Observable<PilotSkill[]>;
  answers$: Observable<Answer[]>;
  application$: Observable<LicenseApplication>;

  clubs$: Observable<any[]> = null;

  firstFormGroup: FormGroup = this.fb.group({});

  constructor(
    private fb: FormBuilder,

    private answerService: AnswerService,
    private licenseApplicationService: LicenseApplicationService,
    private personService: PersonService,
    private pilotSkillService: PilotSkillService,
    private questionService: QuestionService,
    private skillService: SkillService,

    private store: Store<any>
  ) {
    this.questionService.getAll();
    this.skillService.getAll();
    this.skills$ = this.skillService.entities$;

    // authUser
    let lastUser;
    this.authUser$ = this.store.pipe(select(userSelector)).pipe(
      tap(authUser => {
        // will be called once for each subscription
        // only fetch server data once (distinct will not work)
        if (authUser && lastUser !== authUser.payload.sub) {
          lastUser = authUser.payload.sub;
          ClubDataService.handleRequest(this.personService.getByKey(authUser.payload.sub));
          ClubDataService.handleRequest(this.pilotSkillService.getWithQuery({licenseNbr: authUser.payload.lic}));
          ClubDataService.handleRequest(this.answerService.getWithQuery({personId: authUser.payload.sub}));
          ClubDataService.handleRequest(this.licenseApplicationService.getWithQuery({licenseNbr: authUser.payload.lic}));
        }
      })
    );

    // licNbr
    this.licNbr$ = this.authUser$.pipe(
      map((user: AuthUser) => user ? user.payload.lic : null),
    );

    // loggedinPerson
    this.loggedinPerson$ = combineLatest(this.authUser$, this.personService.entityMap$).pipe(
      map(([authUser, personMap]) => (!authUser || !personMap) ? null : personMap[authUser.payload.sub])
    );

  }

  ngOnInit() {
    // application
    this.application$ = combineLatest(this.licNbr$, this.licenseApplicationService.entities$).pipe(
      map(([licNbr, applications]) => {
        if (licNbr && applications) {
          const myApplication = applications.find(application => application.year === this.year && application.licenseNbr === licNbr);
          if (myApplication && this.stepper) {
            this.stepper.steps.last.select();
          }
          return myApplication;
        } else {
          return null;
        }
        })
    );

    this.pilotSkills$ = this.pilotSkillService.entities$;
    this.answers$ = this.answerService.entities$;
  }

  savePerson() {
    if (!this.personForm.valid) {
      // show errors
      this.personForm.markAllAsTouched();
    } else {
      this.stepper.next();
      if (this.personForm.dirty) {
        const tmp = this.personService.update(this.personForm.value).subscribe(_ => {
          this.personForm.markAsPristine();
          tmp.unsubscribe();
        },
        error => {
          console.log(error);
          alert('Ett fel uppstod vid kommunkationen med serven: ' + error.message);
          tmp.unsubscribe();
          });
      }
    }
  }

  saveStatistics() {
    this.statisticComponent.save();
    if (this.statisticsForm.valid) {
      this.stepper.next();
    }
  }

  submitApplication() {
    const request = this.licenseOptionsComponent.submit();
    if (request) {
      const subscription = request.subscribe(x => {
        this.stepper.next();
        subscription.unsubscribe();
      }, error => {
        console.log(error);
        alert('Ett fel uppstod vid kommunkationen med serven: ' + error.message);
        subscription.unsubscribe();
      });
    } else {
      alert('Ett fel uppstod vid kommunkationen med serven');
    }
  }

  /**
   * returns true iff both person and statistics forms are valid and saved.
   */
  preStepsOk() {
    if (!this.statisticsForm || !this.personForm ) {
      return false;
    }
    return this.statisticsForm.valid && this.statisticsForm.pristine && this.personForm.valid && this.personForm.pristine;
  }
}
