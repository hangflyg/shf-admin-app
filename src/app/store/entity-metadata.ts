import { EntityMetadataMap, DefaultHttpUrlGenerator, EntityHttpResourceUrls } from '@ngrx/data';

import { License, PilotSkill } from 'src/app/types/license';
import { Answer, answerKey } from '../types/statistics';

import { AnswerService } from './services/answer.service';
import { ClubService } from './services/club.service';
import { LicenseService } from './services/license.service';
import { LicenseApplicationService } from './services/license-application.service';
import { PersonService } from './services/person.service';
import { PilotSkillService } from './services/pilot-skill.service';
import { QuestionService } from './services/question.service';
import { SkillService } from './services/skill.service';
import { IncidentReport } from '../types/incident-report';

export function answerSelectId(a: Answer) {
  return answerKey(a);
}

export function licenseSelectId(a: License) {
  return a.licenseNbr;
}

export const entityMetadata: EntityMetadataMap = {
  Answer: { selectId: answerSelectId },
  Club: {},
  IncidentReport: {},
  License: { selectId: licenseSelectId },
  LicenseApplication: {},
  Person: {},
  PilotSkill: {},
  Question: {},
  Skill: {}
};
