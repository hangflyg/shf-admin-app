import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PilotSkillComponent } from './pilot-skill.component';

describe('PilotSkillComponent', () => {
  let component: PilotSkillComponent;
  let fixture: ComponentFixture<PilotSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotSkillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PilotSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
