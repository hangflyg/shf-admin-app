import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClubAdminApplicationComponent } from './club-admin-applications.component';

describe('ClubAdminApplicationComponent', () => {
  let component: ClubAdminApplicationComponent;
  let fixture: ComponentFixture<ClubAdminApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClubAdminApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClubAdminApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
