import { NgModule } from '@angular/core';
import { EntityDataModule, DefaultDataServiceConfig } from '@ngrx/data';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { environment } from 'src/environments/environment';
import { entityMetadata } from './entity-metadata';
import { reducers, metaReducers } from '../reducers';
import { PilotSkillService } from './services/pilot-skill.service';
import { LoadIndicatorComponent } from './load-indicator/load-indicator.component';
import { MaterialModule } from '../material.module';
import { CommonModule } from '@angular/common';

/**
 * To add a new entity collection 3 things are needed. You must use the same entity collection name in all 3 places:
 * 1, Add the name to the EntityMetadataMap (entity-metadata.ts)
 * 2, Add the name to urlMap (entity-metadata.ts)
 * 3, create/register the service, by extending EntityCollectionServiceBase<T> and @Injectable({ providedIn: 'root' })
 */

export const defaultDataServiceConfig: DefaultDataServiceConfig = {
  root: window.location.protocol + '//' + window.location.hostname + '/shf/api/v1/',
  entityHttpResourceUrls: {
    [PilotSkillService.storeName]: {
      entityResourceUrl: '/shf/api/v1/pilot-skill/',
      collectionResourceUrl: '/shf/api/v1/pilot-skills/'
    },
    LicenseApplication: {
      entityResourceUrl: '/shf/api/v1/license-application/',
      collectionResourceUrl: '/shf/api/v1/license-applications/'
    },
    Person: {
      entityResourceUrl: '/shf/api/v1/person/',
      collectionResourceUrl: '/shf/api/v1/people/'
    }
  },
  timeout: 0, // request timeout
};

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot([]),
    EntityDataModule.forRoot({entityMetadata}),
    MaterialModule,
    StoreRouterConnectingModule.forRoot( { stateKey: 'router' } ),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],
  providers: [
    { provide: DefaultDataServiceConfig, useValue: defaultDataServiceConfig }
  ],
  declarations: [LoadIndicatorComponent],
  exports: [
    LoadIndicatorComponent
  ]
})
export class ShfStoreModule { }
