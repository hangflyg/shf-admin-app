import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPilotComponent } from './select-pilot.component';

describe('SelectPilotComponent', () => {
  let component: SelectPilotComponent;
  let fixture: ComponentFixture<SelectPilotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectPilotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPilotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
