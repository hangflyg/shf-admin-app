import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { OfficeRoutingModule } from './office-routing.module';
import { OfficeApplicationComponent } from './office-application/office-application.component';
import { MaterialModule } from '../material.module';
import { PilotSkillComponent } from './pilot-skill/pilot-skill.component';
import { SelectPersonComponent } from './select-person/select-person.component';


@NgModule({
  declarations: [
    OfficeApplicationComponent,
    PilotSkillComponent,
    SelectPersonComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    OfficeRoutingModule,
    ReactiveFormsModule
  ]
})
export class OfficeModule { }
