import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthUser } from '@holmby/auth';
import { Observable, Subscription } from 'rxjs';
import { MatPaginator, MatTableDataSource } from '@angular/material';

import { ClubDataService } from 'src/app/services/club-data.service';
import { IncidentReportService } from 'src/app/store/services/incident.service';
import { IncidentReport } from 'src/app/types/incident-report';
import { Router } from '@angular/router';

@Component({
  selector: 'shf-list-incidents',
  templateUrl: './list-incidents.component.html',
  styleUrls: ['./list-incidents.component.scss']
})
export class ListIncidentsComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  columnsToDisplay = ['date', 'severity', 'location', 'reporter'];
  headersToDisplay = ['date', 'severity', 'location', 'reporter'];
  dataSource = new MatTableDataSource<any>([]);
  incidents$: Observable<IncidentReport[]>;

  authUser$: Observable<AuthUser> = this.prefetchService.authUser$;

  formGroup: FormGroup = this.fb.group({
    year: '',
    severity: ''
  });

  private subsriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private incidentReportService: IncidentReportService,
    private prefetchService: ClubDataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.incidents$ = this.incidentReportService.entities$

    this.subsriptions.push(this.incidents$.subscribe(list => {
      this.dataSource.data = list;
      if (this.paginator) {
        this.dataSource.paginator = this.paginator;
      }
    }));
  }

  ngOnDestroy() {
    this.subsriptions.forEach(s => s.unsubscribe());
    this.subsriptions = [];
  }

  openIncident(incident) {
    this.router.navigate(['incident', incident.id]);
  }

}
