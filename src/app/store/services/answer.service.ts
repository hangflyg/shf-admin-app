import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { Answer } from '../../types/statistics';

@Injectable({ providedIn: 'root' })
export class AnswerService extends EntityCollectionServiceBase<Answer> {
  public static readonly storeName = 'Answer';
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(AnswerService.storeName, serviceElementsFactory);
  }
}
