import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { combineLatest } from 'rxjs';
import { startWith, map, shareReplay, filter, tap } from 'rxjs/operators';

import { LicenseService } from 'src/app/store/services/license.service';
import { PersonService } from 'src/app/store/services/person.service';
import { Person } from 'src/app/types/person';

@Component({
  selector: 'shf-select-person',
  templateUrl: './select-person.component.html',
  styleUrls: ['./select-person.component.scss']
})
export class SelectPersonComponent implements OnInit {
  @Output() person = new EventEmitter<Person>();

  formGroup: FormGroup = this.fb.group({
    licNbr: '',
    pnr: '',
    givenName: '',
    familyName: '',
    mobile: '',
    email: ''
  });

  formValues$ = this.formGroup.valueChanges.pipe(
    startWith({}),
    // trim all filter values
    map(value => ({
      ...value,
      licNbr: this.trim(value.licNbr),
      pnr: this.trim(value.pnr),
      givenName: this.trim(value.givenName),
      familyName: this.trim(value.familyName),
      mobile: this.trim(value.mobile),
      email: this.trim(value.email)
    }))
  );
  private peopleList = [];

  filteredPeople$ = combineLatest(this.personService.entities$, this.licenseService.entityMap$, this.formValues$).pipe(
    // filter people
    map(([people, licenseMap, filter]) => {
      if(filter.licNbr) {
        const lic = licenseMap[filter.licNbr];
        if(!lic) {
          return [];
        }
        const personId = lic.personId;
        return [people.find(person => person.id == personId)];
      } else {
        return people.filter(person => this.matches(person, filter));
      } 
    }),
    tap(list => { this.peopleList = list; }),
    shareReplay()
  );

  givenNames$ = this.filteredPeople$.pipe(
    map(people => people.map(person => person.givenName)),
    map(list => Array.from(new Set(list)).sort((a, b) => a.localeCompare(b, 'sv')))
  );

  familyNames$ = this.filteredPeople$.pipe(
    map(people => people.map(person => person.familyName)),
    map(list => Array.from(new Set(list)).sort((a, b) => a.localeCompare(b, 'sv')))
  );

  mobiles$ = this.filteredPeople$.pipe(
    map(people => people.filter(person => person.mobile)),
    map(peopel => peopel.map(person => person.mobile).sort((a, b) => a.localeCompare(b, 'sv')))
  );

  emails$ = this.filteredPeople$.pipe(
    map(people => people.filter(person => person.email)),
    map(peopel => peopel.map(person => person.email).sort((a, b) => a.localeCompare(b, 'sv')))
  );

  constructor(
    private fb: FormBuilder,
    private licenseService: LicenseService,
    private personService: PersonService
  ) {  }

  ngOnInit() {  }

  onSubmit() {
    this.person.emit(this.peopleList[0]);
  }
  matchText(list: Person[]) {
    if(list.length !== 1) {
      return list.length + ' personer';
    } 
    const person = list[0];
    return person.givenName + ' ' + person.familyName;
  }

  private trim(value) {
    return value ? value.trim().toLowerCase() : null;
  }

  private matches(person: Person, filter): boolean {
    if (filter.pnr && !person.personNumber.startsWith(filter.pnr)) {
      return false;
    }
    if (filter.givenName && !person.givenName.toLowerCase().startsWith(filter.givenName)) {
      return false;
    }
    if (filter.familyName && !person.familyName.toLowerCase().startsWith(filter.familyName)) {
      return false;
    }
    if (filter.mobile && (!person.mobile || !person.mobile.startsWith(filter.mobile))) {
      return false;
    }
    if (filter.email && (!person.email || !person.email.toLowerCase().startsWith(filter.email))) {
      return false;
    }
    return true;
  }
}
