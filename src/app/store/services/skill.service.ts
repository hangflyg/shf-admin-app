import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { Skill } from '../../types/license';

@Injectable({ providedIn: 'root' })
export class SkillService extends EntityCollectionServiceBase<Skill> {
  public static storeName = 'Skill';
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(SkillService.storeName, serviceElementsFactory);
  }
}
