import { Injectable } from '@angular/core';
import { AuthUser, userSelector } from '@holmby/auth';
import { Store, select } from '@ngrx/store';
import { Observable, combineLatest } from 'rxjs';
import { tap, shareReplay, map } from 'rxjs/operators';
import { LicenseService } from '../store/services/license.service';
import { LicenseApplicationService } from '../store/services/license-application.service';
import { PersonService } from '../store/services/person.service';
import { LicenseApplication } from '../types/license';
import { Person } from '../types/person';
import { Club } from '../types/club';
import { ClubService } from '../store/services/club.service';
import { PilotSkillService } from '../store/services/pilot-skill.service';
import { SkillService } from '../store/services/skill.service';
import { IncidentReportService } from '../store/services/incident.service';

@Injectable({ providedIn: 'root' })
export class ClubDataService {

  /**
   * Prefetch data when a user logs in.
   */
  public authUser$: Observable<AuthUser> = this.store.pipe(
    select(userSelector),
    tap(authUser => {
      if (authUser && (authUser.privileges.clubadmin || authUser.privileges.office)) {
        // club or office, prefetch all
       ClubDataService.handleRequest(this.licenseApplicationService.getAll());
       ClubDataService.handleRequest(this.personService.getAll());
       ClubDataService.handleRequest(this.licenseService.getAll());
       ClubDataService.handleRequest(this.skillService.getAll());
      } else if (authUser && (authUser.payload.lic)) {
        // pilot
        const personId = authUser.user;
        const licenseNbr = authUser.payload.lic;
        ClubDataService.handleRequest(this.personService.getByKey(personId));
        ClubDataService.handleRequest(this.pilotSkillService.getWithQuery({licenseNbr}));
        ClubDataService.handleRequest(this.licenseApplicationService.getWithQuery({licenseNbr}));
      }
      if (authUser && (authUser.privileges.safty)) {
        // incident reports
        ClubDataService.handleRequest(this.incidentReportService.getAll());
     }
    }),
    shareReplay()
  );


  /**
   * List of license applications to be processed by the logged in person
   */
  public clubApplications$: Observable<{application: LicenseApplication, person: Person}[]> =
  combineLatest(
    this.authUser$,
    this.licenseApplicationService.entities$,
    this.personService.entityMap$,
    this.licenseService.entityMap$
  ).pipe(
    map(([user, applicationList, peopleMap, licenseMap]) => {
      if (!user || !applicationList || !peopleMap || !licenseMap) {
        return [];
      }
      // filter out applications for my clubs, that have not been handeld
      const adminClubIds = user.privileges.clubadmin.map(id => +id);
      const list = applicationList.filter(application => !application.timestampClub && adminClubIds.includes(+application.clubId));
      const pairs = ClubDataService.pairApplicationPerson(list, licenseMap, peopleMap);
      return pairs.sort( (a, b) => this.strCmp(a.application.timestampPilot, b.application.timestampPilot));
    }),
    shareReplay()
  );

  /**
   * List of license applications to be processed by the office
   */
  public officeApplications$: Observable<{application: LicenseApplication, person: Person, club: Club}[]> =
  combineLatest(
    this.authUser$,
    this.licenseApplicationService.entities$,
    this.personService.entityMap$,
    this.licenseService.entityMap$,
    this.clubService.entityMap$
  ).pipe(
    map(([user, applicationList, peopleMap, licenseMap, clubMap]) => {
      if (!user || !applicationList || !peopleMap || !licenseMap) {
        return [];
      }
      // filter out applications that have not been handeld
      const list = applicationList.filter(application => !application.timestampOffice);
      // pair application and person objects
      return ClubDataService.pairApplicationPerson(list, licenseMap, peopleMap)
      .map(data => ({...data, club: clubMap[data.application.clubId]}))
      .sort((a, b) => this.officeSortOrder(a, b));
    }),
    shareReplay()
  );

  static handleRequest(req: Observable<any>) {
    const subscr = req.subscribe(
      _ => subscr.unsubscribe(),
      error => {
        console.log(error);
        alert('Ett fel uppstod vid kommunkationen med serven: ' + error.message);
        subscr.unsubscribe();
      });
  }

  /**
   * Returns a list of matchin {application, person} objects
   */
   static pairApplicationPerson(applications: LicenseApplication[], licenseMap, peopleMap):
   {application: LicenseApplication, person: Person}[] {
    return applications.map( application => {
      let person = null;
      const licenseNbr = application.licenseNbr;
      const licens = licenseMap[licenseNbr];
      const personId = licens ? licens.personId : null;
      person = personId ? peopleMap[personId] : null;
      return {application, person};
    });
  }

  constructor(
    private clubService: ClubService,
    private incidentReportService: IncidentReportService,
    private licenseService: LicenseService,
    private licenseApplicationService: LicenseApplicationService,
    private personService: PersonService,
    private pilotSkillService: PilotSkillService,
    private skillService: SkillService,
    private store: Store<any>
  ) { }

  officeSortOrder(a, b): number {
    if (a.application.timestampClub != null) {
      if (b.application.timestampClub != null) {
        return this.cmpPerson(a.person, b.person);
      } else {
        return -1;
      }
    } else {
      // a.timestampClub == null
      if (b.application.timestampClub != null) {
        return 1;
      } else {
        return this.cmpPerson(a.person, b.person);
      }
    }
    if (a > b) {
      return -1;
    } else if (b > a) {
      return 1;
    }
    return 0;
  }

  cmpPerson(a, b): number {
    const major = a.familyName.localeCompare(b.familyName);
    return major === 0 ? a.givenName.localeCompare(b.givenName) : major;
  }

  strCmp(a, b): number {
    if (a > b) {
      return -1;
    } else if (b > a) {
      return 1;
    }
    return 0;
  }

}
