import { EntityState } from '@ngrx/entity';

export interface Person {
  id: number;
  personNumber: string;
  givenName: string;
  familyName: string;
  street: string;
  zip: string;
  city: string;
  country: string;
  mobile: string;
  email: string;
  protectedIdentity: number; // boolean;
  iol: string;
  contactPersonName: string;
  contactPersonPhone: string;
}
