import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';

import { AutoFocusDirective } from './directives/auto-focus.directive';
import { AuthModule, AuthConfig } from '@holmby/auth';

import {
  MatNativeDateModule,
  DateAdapter,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDividerModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  NativeDateAdapter,
  MAT_DATE_LOCALE,
} from '@angular/material';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';

import { ShfStoreModule } from './store/entity-store.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { IncidentComponent } from './incident/incident/incident.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ListIncidentsComponent } from './incident/list-incidents/list-incidents.component';

const authConfig: AuthConfig = {
  persistent: true,               // store login info in localstore (restore login state when reloading page)
  inactivWarningTime: 25 * 60 * 1000,    // after inactivWarningTime miliseconds of inactivity a warning is shown
  inactivLogoutTime: 30 * 60 * 1000,     // after inactivLogoutTime miliseconds of inactivity the user is logged out
  text: {
    loginUser: 'personnummer (yymmddnnnn)',
    loginPassword: 'licensnummer',
  },
  url: {
    login: '/shf/api/v1/login/',
    forgotPassword: '/shf/api/v1/forgot-password/',
  }
};

@NgModule({
  declarations: [
    AppComponent,
    AutoFocusDirective,
    IncidentComponent,
    ListIncidentsComponent,
    NavigationComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,

    LayoutModule,
    MatBadgeModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDividerModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatToolbarModule,
    MatTabsModule,

    ShfStoreModule,
    AuthModule.forRoot(authConfig),
  ],
  providers: [
    {provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}},
    {provide: MAT_DATE_LOCALE, useValue: 'sv-SV'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
