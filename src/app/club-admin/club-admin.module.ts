import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../material.module';
import { ClubAdminApplicationComponent } from './club-admin-applications/club-admin-applications.component';
import { ClubAdminRoutingModule } from './club-admin-routing.module';


@NgModule({
  declarations: [
    ClubAdminApplicationComponent
  ],
  imports: [
    CommonModule,
    ClubAdminRoutingModule,
    MaterialModule
  ],
  exports: [
    ClubAdminApplicationComponent
  ]
})
export class ClubAdminModule { }
