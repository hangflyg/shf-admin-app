import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/types/person';
import { LicenseService } from 'src/app/store/services/license.service';
import { ClubDataService } from 'src/app/services/club-data.service';
import { Observable } from 'rxjs';
import { AuthUser } from '@holmby/auth';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SkillService } from 'src/app/store/services/skill.service';
import { map } from 'rxjs/operators';
import { PilotSkillService } from 'src/app/store/services/pilot-skill.service';
import { PilotSkill } from 'src/app/types/license';

@Component({
  selector: 'shf-pilot-skill',
  templateUrl: './pilot-skill.component.html',
  styleUrls: ['./pilot-skill.component.scss']
})
export class PilotSkillComponent implements OnInit {
  authUser$: Observable<AuthUser> = this.prefetchService.authUser$;
  pilot: Person & {licenseNbr: number} = null;
  skills$ = this.skillService.entities$.pipe(
    map(list => list.sort((a, b) => a.swedish.localeCompare(b.swedish, 'sv')))
  );

  formGroup: FormGroup = this.fb.group({
    instrLicNbr: ['', Validators.required],
    date: [new Date().toLocaleDateString('sv'), Validators.required],
    skillId: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private skillService: SkillService,
    private pilotSkillService: PilotSkillService,
    private prefetchService: ClubDataService,
    private licenseService: LicenseService
  ) { }

  ngOnInit() {
  }

  onPerson(person: Person) {
    this.licenseService.personMap$.subscribe(map => {
      this.pilot = {...person, licenseNbr: map[person.id]};
    });

  }

  createPilotSkill() {
    const pilotSkill: PilotSkill = {
      id: null,
      licenseNbr: this.pilot.licenseNbr,
      skillId: this.formGroup.value.skillId,
      issueDate: this.formGroup.value.date,
      revokeDate: null,
      issuingInstructorLicNbr: this.formGroup.value.instrLicNbr
    }
    this.pilotSkillService.add(pilotSkill);
  }
}
