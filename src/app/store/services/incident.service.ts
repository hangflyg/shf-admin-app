import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { IncidentReport } from 'src/app/types/incident-report';

@Injectable({ providedIn: 'root' })
export class IncidentReportService extends EntityCollectionServiceBase<IncidentReport> {
  public static storeName = 'IncidentReport';
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(IncidentReportService.storeName, serviceElementsFactory);
  }
}
