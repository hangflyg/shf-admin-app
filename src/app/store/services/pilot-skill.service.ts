import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { PilotSkill } from '../../types/license';

@Injectable({ providedIn: 'root' })
export class PilotSkillService extends EntityCollectionServiceBase<PilotSkill> {
  public static storeName = 'PilotSkill';
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(PilotSkillService.storeName, serviceElementsFactory);
  }
}
