import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { map, filter, shareReplay, startWith } from 'rxjs/operators';

import { AuthUser } from '@holmby/auth';

import { LicenseApplication } from 'src/app/types/license';
import { Person } from 'src/app/types/person';

import { ClubDataService } from 'src/app/services/club-data.service';
import { LicenseApplicationService } from 'src/app/store/services/license-application.service';
import { Club } from 'src/app/types/club';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ClubService } from 'src/app/store/services/club.service';
import { LicenseService } from 'src/app/store/services/license.service';
import { PersonService } from 'src/app/store/services/person.service';
import { FormGroup, FormBuilder } from '@angular/forms';

interface AugmentedApplication {
  application: LicenseApplication;
  person: Person;
  club: Club;
}

const constants = {approve: 'a', reject: 'r', undecided: 'n', decided: 'd'};

@Component({
  selector: 'shf-office-application',
  templateUrl: './office-application.component.html',
  styleUrls: ['./office-application.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class OfficeApplicationComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  columnsToDisplay = ['approve', 'reject', 'flight-test', 'club-status', 'club', 'year', 'level', 'name', 'licnbr', 'comment'];
  headersToDisplay = ['approve', 'reject', 'flight-test', 'club-status', 'year', 'level', 'name', 'licnbr', 'comment'];
  expandedApplication: any | null;
  dataSource = new MatTableDataSource<any>([]);
  constants = constants;

  formGroup: FormGroup = this.fb.group({
    year: '',
    office: constants.undecided,
    club: null,
    licenseNbr: null
  });

  authUser$: Observable<AuthUser> = this.prefetchService.authUser$;
  selectedApplications$: Observable<LicenseApplication[]>;
  applications$: Observable<AugmentedApplication[]>;
  applicationCount$;

  private subsriptions: Subscription[] = [];

  constructor(
    private clubService: ClubService,
    private licenseService: LicenseService,
    private licenseApplicationService: LicenseApplicationService,
    private personService: PersonService,
    private prefetchService: ClubDataService,
    private fb: FormBuilder
  ) {
    this.filter.bind(this);
  }

  ngOnInit() {
    // --- selectedApplications$ ---
    this.selectedApplications$ = combineLatest(
      this.licenseApplicationService.entities$,
      this.formGroup.valueChanges.pipe(startWith(this.formGroup.value))
    ).pipe(
      map(this.filter),
    );

    // --- applications$ ---
    this.applications$ = combineLatest(
      this.authUser$,
      this.selectedApplications$,
      this.personService.entityMap$,
      this.licenseService.entityMap$,
      this.clubService.entityMap$
    ).pipe(
      map(([user, applicationList, peopleMap, licenseMap, clubMap]) => {
        if (!user || !applicationList || !peopleMap || !licenseMap) {
          return [];
        }
        // pair application and person objects
        return ClubDataService.pairApplicationPerson(applicationList, licenseMap, peopleMap)
        .map(data => ({...data, club: clubMap[data.application.clubId]}))
        .sort((a, b) => this.officeSortOrder(a, b));
      }),
      shareReplay()
    );

    // --- applicationCount$ ---
    this.applicationCount$ = this.applications$.pipe(
      map(list => list.length)
    );

    this.subsriptions.push(this.applications$.subscribe(list => {
      this.dataSource.data = list;
      if (this.paginator) {
        this.dataSource.paginator = this.paginator;
      }
    }));
  }

  ngOnDestroy() {
    this.subsriptions.forEach(s => s.unsubscribe());
    this.subsriptions = [];
  }

  handleApplication(data, approve) {
    const application = {
      id: data.application.id,
      timestampOffice: new Date().toISOString().slice(0, 19).replace('T', ' '),
      approvedOffice: approve
    };
    ClubDataService.handleRequest(this.licenseApplicationService.update(application));
  }

  officeSortOrder(a, b): number {
    if (a.application.timestampClub != null) {
      if (b.application.timestampClub != null) {
        return this.cmpPerson(a.person, b.person);
      } else {
        return -1;
      }
    } else {
      // a.timestampClub == null
      if (b.application.timestampClub != null) {
        return 1;
      } else {
        return this.cmpPerson(a.person, b.person);
      }
    }
    if (a > b) {
      return -1;
    } else if (b > a) {
      return 1;
    }
    return 0;
  }

  cmpPerson(a: Person, b: Person): number {
    if (!a || !b) {
      return 0;
    }
    const major = a.familyName.localeCompare(b.familyName);
    return major === 0 ? a.givenName.localeCompare(b.givenName) : major;
  }

  approveButtonColor(data: AugmentedApplication) {
    // TODO, check payments
    if (data.application.timestampClub &&
      (!data.application.needFlightTest || data.application.timestampInstructor)) {
    return 'primary';
    }
    return '';
  }

  filter([applicationList, filterValues]: [LicenseApplication[], any]): LicenseApplication[] {
    let result = [];
    if (applicationList) {
      result = applicationList.filter(application => {
        // --- office ---
        if (filterValues.office === constants.undecided && application.timestampOffice) { return false; }
        if (filterValues.office === constants.decided && !application.timestampOffice) { return false; }
        if (filterValues.office === constants.approve && !(application.approvedOffice > 0)) { return false; }
        if (filterValues.office === constants.reject && !(application.approvedOffice === 0)) { return false; }
        // --- club ---
        if (filterValues.club === constants.undecided && application.timestampClub) { return false; }
        if (filterValues.club === constants.decided && !application.timestampClub) { return false; }
        if (filterValues.club === constants.approve && !(application.approvedClub > 0)) { return false; }
        if (filterValues.club === constants.reject && !(application.approvedClub === 0)) { return false; }
        // --- year ---
        const year = +filterValues.year;
        if (year && !(year === +application.year)) { return false; }
        // --- pilot ---
        const licenseNbr = +filterValues.licenseNbr;
        if (licenseNbr && !(licenseNbr === +application.licenseNbr)) { return false; }
        return true;
      });
    }
    return result;
  }
}
