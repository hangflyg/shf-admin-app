import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeApplicationComponent } from './office-application.component';

describe('OfficeApplicationComponent', () => {
  let component: OfficeApplicationComponent;
  let fixture: ComponentFixture<OfficeApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficeApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
