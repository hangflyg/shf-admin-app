import { Component, OnInit, Input, OnChanges, SimpleChange, ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import { Skill, PilotSkill } from 'src/app/types/license';
import { Observable, combineLatest } from 'rxjs';
import { filter, switchMap, map } from 'rxjs/operators';


import { PilotSkillService } from 'src/app/store/services/pilot-skill.service';
import { MatTableDataSource, MatTable } from '@angular/material';
import { SkillService } from 'src/app/store/services/skill.service';
import { Person } from 'src/app/types/person';

@Component({
  selector: 'shf-pilot-skills',
  templateUrl: './pilot-skills.component.html',
  styleUrls: ['./pilot-skills.component.css']
})
export class PilotSkillsComponent implements OnInit, OnChanges {
  @Input() licNbr$: Observable<number>;

  displayedColumns = ['swedish', 'issue-date'];
  dataSource$;

  constructor(
    private skillService: SkillService,
    private pilotSkillService: PilotSkillService
  ) { }

  ngOnInit() {
    this.dataSource$ = combineLatest(this.skillService.entityMap$, this.pilotSkillService.entities$, this.licNbr$).pipe(
      filter(([skillMap, pilotSkills, licNbr]) => !!skillMap && !!pilotSkills && !!licNbr && Object.keys(skillMap).length > 1),
      map(([skillMap, pilotSkills, licNbr]) => pilotSkills
          .filter(skill => skill.licenseNbr === licNbr)
          .map(skill => ({...skill, swedish: skillMap[skill.skillId].swedish}))
          .sort((a, b) => {
            if (!a || !b || a.swedish === b.swedish) { return 0; }
            return a.swedish > b.swedish ? 1 : -1;
          })
      )
    );

  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
//    if (this.skills$ && this.licNbr) {
//      this.dataSource = this.skills$;
//    }
  }
}
