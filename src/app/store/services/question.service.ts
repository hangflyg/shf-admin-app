import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { Question } from '../../types/statistics';

@Injectable({ providedIn: 'root' })
export class QuestionService extends EntityCollectionServiceBase<Question> {
  public static storeName = 'Question';
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(QuestionService.storeName, serviceElementsFactory);
  }
}
