import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';

export interface State {
  person: any;
}

export const reducers: ActionReducerMap<State> = {
  person: null
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
