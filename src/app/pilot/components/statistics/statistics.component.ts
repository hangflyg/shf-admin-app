import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { map, share, filter } from 'rxjs/operators';

import { Person } from 'src/app/types/person';
import { AnswerService } from 'src/app/store/services/answer.service';

@Component({
  selector: 'shf-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  @Input() year;
  @Input() person$: Observable<Person>;

  answers$;
  serverlValues = {};

  formGroup: FormGroup = this.fb.group({
    person: [null],
    1: [null, Validators.required],
    2: [null],
    3: [null, Validators.required],
    4: [null],
    5: [null, Validators.required],
    6: [null],
    7: [null, Validators.required],
    8: [null],
    9: [null, Validators.required],
  });
  // TODO fetch the set of questions from the server
  questions = [{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 6}, {id: 7}, {id: 8}, {id: 9}];

  constructor(
    private fb: FormBuilder,
    public answerService: AnswerService
  ) { }

  ngOnInit() {
    this.answers$ = combineLatest(this.answerService.entities$, this.person$).pipe(
      map(([list, person]) => {
        const result = {};
        if (list && person) {
          this.formGroup.controls.person.setValue(person.id);
          list.forEach(answer => {
            if (+answer.personId === +person.id) {
              if (answer.year === this.year) {
                const value = {};
                value[answer.qId] = answer.value;
                this.serverlValues[answer.qId] = answer;
                this.formGroup.patchValue(value);
              } else if ((!result[answer.qId] || result[answer.qId].year < answer.year)
                         && answer.year < this.year) {
                result[answer.qId] = answer;
              }
            }
          });
        }
        return result;
      }),
      share()
    );
  }

  makeStat(answer, index) {
    if (answer && answer[index]) {
      return '(' + answer[index].value + ' rapporterade ' + answer[index].year + ')';
    } else {
      return '';
    }
  }

  save() {
    const personId = this.formGroup.controls.person.value;
    this.questions.forEach(q => {
      const control = this.formGroup.controls[q.id];
      if (!control.valid) {
        // show error
        this.formGroup.markAllAsTouched();
      } else {
        // save value
        const newAnswer = {
          qId: q.id,
          year: this.year,
          personId,
          text: control.value,
          value: control.value
        };
        if (this.serverlValues.hasOwnProperty(q.id)) {
          if (newAnswer.value !== this.serverlValues[q.id].value) {
            // value changed, update server
            const tmp = this.answerService.update(newAnswer).subscribe(_ => {
              this.serverlValues[q.id] = newAnswer.value;
              control.markAsPristine();
              tmp.unsubscribe();
            });
          } else {
            control.markAsPristine();
          }
        } else {
          if (newAnswer.value != null) {
            // new value, save on server
            const tmp = this.answerService.add(newAnswer).subscribe(_ => {
              this.serverlValues[q.id] = newAnswer.value;
              control.markAsPristine();
              tmp.unsubscribe();
            });
          }
        }
      }
    });
  }

}
