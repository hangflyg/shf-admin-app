import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PilotSkillsComponent } from './pilot-skills.component';

describe('PilotSkillsComponent', () => {
  let component: PilotSkillsComponent;
  let fixture: ComponentFixture<PilotSkillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotSkillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PilotSkillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
