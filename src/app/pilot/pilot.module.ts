import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { PilotRoutingModule } from './pilot-routing.module';
import { MaterialModule } from '../material.module';
import { LicenseComponent } from './license/license.component';
import { RenewLicenseComponent } from './renew-license/renew-license.component';
import { LicenseOptionsComponent } from './components/license-options/license-options.component';
import { PersonComponent } from './components/person/person.component';
import { PilotSkillsComponent } from './components/pilot-skills/pilot-skills.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { LicensApplicationStatusComponent } from './components/licens-application-status/licens-application-status.component';


@NgModule({
  declarations: [
    LicenseComponent,
    RenewLicenseComponent,
    LicenseOptionsComponent,
    PersonComponent,
    PilotSkillsComponent,
    StatisticsComponent,
    LicensApplicationStatusComponent

  ],
  imports: [
    CommonModule,
    PilotRoutingModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class PilotModule { }
