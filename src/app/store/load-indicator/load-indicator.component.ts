import { Component, OnInit } from '@angular/core';
import { EntityServices } from '@ngrx/data';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { entityMetadata } from '../entity-metadata';

@Component({
  selector: 'shf-load-indicator',
  templateUrl: './load-indicator.component.html',
  styleUrls: ['./load-indicator.component.scss']
})
export class LoadIndicatorComponent implements OnInit {
  loading$;

  constructor(private entityServices: EntityServices) { }

  ngOnInit() {

    const services: Observable<boolean>[] = [];
    Object.keys(entityMetadata).forEach(serviceName => {
      services.push(this.entityServices.getEntityCollectionService(serviceName).loading$);
      this.loading$ = combineLatest(services).pipe(
        map(list => list.reduce((prev, value) => (value || prev) ))
      );
    });
 }

}
