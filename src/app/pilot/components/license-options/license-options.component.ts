import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChange } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Observable, combineLatest, Subscription } from 'rxjs';
import { filter, map, share, tap } from 'rxjs/operators';

import { Club } from 'src/app/types/club';
import { ClubService } from 'src/app/store/services/club.service';
import { Person } from 'src/app/types/person';
import { PilotSkillService } from 'src/app/store/services/pilot-skill.service';
import { SkillService } from 'src/app/store/services/skill.service';
import { AnswerService } from 'src/app/store/services/answer.service';
import { LicenseApplicationService } from 'src/app/store/services/license-application.service';
import { LicenseApplication } from 'src/app/types/license';

interface Statistics {
  startsLastYear: number;
  flightTimeLastYear: number;
  flightTimeTwoYearsAgo: number;
  totalFlightTime: number;
}

interface RenewData extends Statistics {
  renewAlternative: number;
  prerequisiteElev: boolean;
  prerequisiteTimePilot: boolean;
  canUse80HoursException: boolean;
}

@Component({
  selector: 'shf-license-options',
  templateUrl: './license-options.component.html',
  styleUrls: ['./license-options.component.css']
})
export class LicenseOptionsComponent implements OnInit, OnDestroy, OnChanges {
  @Input() year;
  @Input() person$: Observable<Person>;
  @Input() licNbr$: Observable<number>;
  @Input() application$: Observable<LicenseApplication>;
  clubs$: Observable<Club[]>;

  readonly renewAlternatives = {elev: 1, pilot: 2, pilotDowngradToElev: 3, pilot80housException: 4, uppflygning: 5};
  readonly timeQId = 1;
  readonly startsQId = 5;
  readonly totalTimeQId = 3;
  readonly minStarts = 10;
  readonly minPilotFlightTime = 5;
  readonly minTotalFlightTime80HoursException = 80;

  private subscriptions: Subscription[] = [];
  isPilot$: Observable<boolean>;
  statistics$: Observable<Statistics>;
  renewData: RenewData;
  isPilot: boolean;
  level: string;

  formGroup: FormGroup = this.fb.group({
    clubId: [null, Validators.required],
    downgradeToElev: false,
    needFlightTest: false,
    comment: null,
    licenseNbr: [null, Validators.required],
    year: [null, Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    private answerService: AnswerService,
    private clubService: ClubService,
    private licenseApplicationService: LicenseApplicationService,
    private pilotSkillService: PilotSkillService,
    private skillService: SkillService,
  ) { }


  ngOnInit() {
    // --- application$
    this.subscriptions.push(this.application$.subscribe(application => {
      if (application) {
        this.formGroup.patchValue({
          clubId: application.clubId,
          downgradeToElev: application.downgradeToElev,
          needFlightTest: application.needFlightTest,
          comment: application.comment,
          licenseNbr: application.licenseNbr,
          year: application.year,
        });
        this.formGroup.disable();
      }
    }));

    // --- clubs$
    this.clubs$ = this.clubService.entities$.pipe(
      map(list => list.filter(club => club.active).sort((a, b) => a.name.localeCompare(b.name)))
    );

    // --- isPilot$
    this.isPilot$ = combineLatest(this.skillService.entities$, this.pilotSkillService.entities$, this.licNbr$).pipe(
      filter(([skills, pilotSkills, licNbr]) => !!skills && !!pilotSkills && !!licNbr),
      map(([skills, pilotSkills, licNbr]) => {
        this.formGroup.patchValue( { licenseNbr: licNbr} );
        const pilotSkillId = skills.find(skill => skill.swedish === 'pilotlicens').id;
        const pilotSkill = pilotSkills.find(ps => ps.licenseNbr === licNbr && ps.skillId === pilotSkillId && ps.revokeDate === null);
        this.level = pilotSkill ? 'Pilot' : 'Elev';
        return !!pilotSkill;
      }),
      share()
    );

    // --- statistics$
    this.statistics$ = combineLatest(this.answerService.entities$, this.person$).pipe(
      map(([list, person]) => {
        const statistics = {
          startsLastYear: 0,
          flightTimeLastYear: 0,
          flightTimeTwoYearsAgo: 0,
          totalFlightTime: 0,
          totalFlightTimeYear: 0
        };
        if (list && person) {
          list.forEach(answer => {
            if (+answer.personId === +person.id) {
              if (answer.year === this.year - 1) {
                // last year
                if (answer.qId === this.startsQId) {
                  // nbr starts last year
                  statistics.startsLastYear = answer.value;
                } else if (answer.qId === this.timeQId) {
                  // flight time last year
                  statistics.flightTimeLastYear = answer.value;
                }
              } else if (answer.year === this.year - 2) {
                // flight time two years ago
                if (answer.qId === this.timeQId) {
                  statistics.flightTimeTwoYearsAgo = answer.value;
                }
              }
              if (answer.qId === this.totalTimeQId) {
                // total flight time
                if (answer.year > statistics.totalFlightTimeYear) {
                  statistics.totalFlightTime = answer.value;
                  statistics.totalFlightTimeYear = answer.year;
                }
              }
            }
          });
        }
        return statistics;
      }),
      share()
    );

    /**
     * The code bellow checks the prerequisits for licnese renwal
     */
    this.subscriptions.push(combineLatest(this.statistics$, this.isPilot$).pipe(
      map(([statistics, isPilot]) => {
        this.isPilot = isPilot;
        const result = {
          ...statistics,
          renewAlternative: undefined,
          prerequisiteElev: false,
          prerequisiteTimePilot: false,
          canUse80HoursException: false
        };
        result.prerequisiteElev = statistics.startsLastYear >= this.minStarts;
        if (!isPilot) {
          result.renewAlternative = result.prerequisiteElev ? this.renewAlternatives.elev : this.renewAlternatives.uppflygning;
        } else {
          result.prerequisiteTimePilot = statistics.flightTimeLastYear >= this.minPilotFlightTime;
          // 80 hours exception, can be used every second year when a total of 80 hours has been reached
          result.canUse80HoursException =
            (statistics.flightTimeTwoYearsAgo >= this.minPilotFlightTime) &&
            (statistics.totalFlightTime >= this.minTotalFlightTime80HoursException);
          if (result.prerequisiteElev && result.prerequisiteTimePilot) {
            result.renewAlternative = this.renewAlternatives.pilot;
          } else if (result.canUse80HoursException) {
            result.renewAlternative = this.renewAlternatives.pilot80housException;
          } else if (result.prerequisiteElev) {
            result.renewAlternative = this.renewAlternatives.pilotDowngradToElev;
          } else {
            result.renewAlternative = this.renewAlternatives.uppflygning;
          }
        }
        return result;
      })
    ).subscribe( x => {
      this.renewData = x;
      if (x.renewAlternative === this.renewAlternatives.pilotDowngradToElev) {
        this.formGroup.controls.downgradeToElev.setValue(true);
      } else if (x.renewAlternative === this.renewAlternatives.uppflygning) {
        this.formGroup.controls.needFlightTest.setValue(true);
      }
    }));

  }

  ngOnDestroy() {
    this.subscriptions.forEach( x => x.unsubscribe());
    this.subscriptions = [];
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    const year = changes.year;
    if (year) {
      this.formGroup.controls.year.setValue(year.currentValue);
    }
  }

  submit(): Observable<LicenseApplication> {
    let result = null;
    if (!this.formGroup.valid) {
      // show errors
      this.formGroup.markAllAsTouched();
    } else {
      if (this.formGroup.dirty) {
        const value = { ...this.formGroup.value,
          id: null,
          level: this.level,
          timestampPilot: new Date().toISOString().slice(0, 19).replace('T', ' '),
          timestampClub: null,
          approvedClub: null,
          timestampOffice: null,
          approvedOffice: null,
          timestampInstructor: null,
          commentOffice: null,
          downgradeToElev: this.formGroup.value.downgradeToElev ? 1 : 0,
          needFlightTest: this.formGroup.value.needFlightTest  ? 1 : 0
        };
        result = this.licenseApplicationService.add(value).pipe(
          tap(_ => this.formGroup.markAsPristine())
        );
      } else {
        console.log('form is not dirty, nothing new to submit');
      }
    }
    return result;
  }
}
